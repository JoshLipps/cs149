package edu.sjsu.cs;

/**
 * I am a Task, please complete me!
 * 
 * @author Adrian And Josh
 *
 */
public class Task implements Comparable<Task> {
	float arrivalTime=0; //this being a float is dumb it might as well be a int but spec requires it
	int runtime=0;
	int priority=4; // Range 1 to 4, 1 being the highest priority
	String name="";
	int totalRunTime = 0;
	int totalWait = 0;
	int response = 0;
	int promote = 0;
	
	/**
	 * @param arrivalTime
	 * @param runtime
	 * @param priority
	 */
	Task(float arrivalTime, int runtime, int priority,String name)
	{
		this.arrivalTime = arrivalTime;
		this.runtime = runtime;
		this.priority = priority; 
		this.name = name;
	}
	
	/**
	 * @param arrivalTime
	 * @param runtime
	 */
	Task(float arrivalTime, int runtime)
	{
		this.arrivalTime = arrivalTime;
		this.runtime = runtime;
		this.priority = 4; 
	}

	/**
	* returns true if done
	*/
	public boolean step(){
		if(totalRunTime == 0)
			response += totalWait;
		totalRunTime++;
		
		return stillRunning();
	}


	public boolean stillRunning(){
		if (totalRunTime >= runtime) 
			return false;
		else
			return true;
	}

	public void waitALittle(){
		totalWait++;
	}

	/**
	 * @return
	 */
	public float getArrivalTime() {
		return arrivalTime;
	}
	
	public int getRuntime() {
		return runtime;
	}

	/**
	 * @return
	 */
	public int getPriority() {
		return priority - promote;
	}


	@Override
	public int compareTo(Task arg0) {
		return Double.compare(this.arrivalTime, arg0.arrivalTime);
	}

	public String getName() {
		return name;
	}

	public int getTotalRunTime() {
		// TODO Auto-generated method stub
		return totalRunTime;
	}
	public int getTotalWait() {
		// TODO Auto-generated method stub
		return totalWait;
	}

	public int getResponseTime() {
		
		return response;
	}
	
	public void promote(){
		if ((priority - promote) > 1 )
			promote++;	
	}
	public int getPromote(){
		return promote;
	}
	
	
}
