package edu.sjsu.cs;

public class ScheduleResults {
	SchedulingAlgorithm[] list;
	float averageTurnaroundTime = 0;
	float averageWaitingTime = 0 ;
	float averageResponseTime = 0;
	float averageThroughput = 0 ;
	String algorithmName = "";
	String plist = "";
	String sliceList = "";
	
	
	
	ScheduleResults(SchedulingAlgorithm[] l){
		list = l;
		algorithmName = l[0].getName();
		calc();
	}
	public String results(){
		String r = "";
		
		r += "Algorithm: "+ algorithmName +"\n";
		//r += "Proccess List: " + plist + "\n";
		r += sliceList + "\n"; 
		r += "AverageTurnaroundTime: " + averageTurnaroundTime + "\n";
		r += "AverageWaitingTime: "+ averageWaitingTime + "\n";
		r += "AverageResponseTime: " + averageResponseTime + "\n";
		r += "AverageThroughput: " + averageThroughput + "\n";
		
		
		return r;
	}
	public void calc(){
		for(int i=0;i<list.length;i++){
			sliceList += "R"+ i + ": " + list[i].getPlist();
			averageTurnaroundTime += list[i].getAverageTurnaroundTime();
			averageWaitingTime += list[i].getAverageWaitingTime();
			averageResponseTime += list[i].getAverageResponseTime();
			averageThroughput += list[i].getAverageThroughput();
			sliceList += list[i].getSlice() + "\n\n";
		}
	}
}
