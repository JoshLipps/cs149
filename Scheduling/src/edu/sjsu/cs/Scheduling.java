package edu.sjsu.cs;

/**
 * @author Adrian & Josh
 *
 */
public class Scheduling {


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int RUNS = 5;
		FirstComeFirstServed[] fcfs = new FirstComeFirstServed[RUNS];
		ShortestJobFirst[] sjf = new ShortestJobFirst[RUNS];
		ShortestRuntime[] srt = new ShortestRuntime[RUNS];
		RoundRobin[] rr = new RoundRobin[RUNS];
		HighestPriorityFirst[] hpf = new HighestPriorityFirst[RUNS];
		HighestPriorityFirstPre[] hpfp = new HighestPriorityFirstPre[RUNS];
		HighestPriorityFirstAge[] hpfa = new HighestPriorityFirstAge[RUNS];
		HighestPriorityFirstPreAge[] hpfpa = new HighestPriorityFirstPreAge[RUNS];
		ScheduleResults[] results = new ScheduleResults[8]; 
		
		

		//OPTIONAL Create 5 seeds so all algorithms act on same results
		for(int i=0;i<RUNS;i++){
			fcfs[i] = new FirstComeFirstServed();
			sjf[i] = new ShortestJobFirst();
			srt[i] = new ShortestRuntime();
			rr[i] = new RoundRobin();
			hpf[i] = new HighestPriorityFirst();
			hpfp[i] = new HighestPriorityFirstPre();
			hpfa[i] = new HighestPriorityFirstAge();
			hpfpa[i] = new HighestPriorityFirstPreAge();
		}
		
		for(int i=0;i<RUNS;i++){
			fcfs[i].run();
			sjf[i].run();
			srt[i].run();
			rr[i].run();
			hpf[i].run();
			hpfp[i].run();
			hpfa[i].run();
			hpfpa[i].run();
		}
		
		results[0] = new ScheduleResults(fcfs);
		results[1] = new ScheduleResults(sjf);
		results[2] = new ScheduleResults(srt);
		results[3] = new ScheduleResults(rr);
		results[4] = new ScheduleResults(hpf);
		results[5] = new ScheduleResults(hpfp);
		results[6] = new ScheduleResults(hpfa);
		results[7] = new ScheduleResults(hpfpa);
		 
		for(int i=0;i<results.length;i++){
			System.out.println(results[i].results());
		}


	}

}
