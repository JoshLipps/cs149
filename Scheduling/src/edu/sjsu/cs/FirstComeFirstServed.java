package edu.sjsu.cs;

public class FirstComeFirstServed extends SchedulingAlgorithm{
	//int currentTask = 0; // Let's start from the beginning
	public FirstComeFirstServed() {
		super();
	}

	public FirstComeFirstServed(long l) {
		super(l);
	}

	@Override
	public Task nextTask() {
		if (runQueue.isEmpty())
			return null;
		else
			return runQueue.get(0);
	}
	public String getName() {
		return "FirstComeFirstServed";
	}

}
