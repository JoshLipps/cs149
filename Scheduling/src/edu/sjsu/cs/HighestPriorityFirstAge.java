/**
 * 
 */
package edu.sjsu.cs;

import java.util.ArrayList;

/**
 * @author adrian
 *
 */
public class HighestPriorityFirstAge extends SchedulingAlgorithm {
	// Here are four priority queues
	ArrayList<Task> priorityOne = new ArrayList<Task>();
	ArrayList<Task> priorityTwo = new ArrayList<Task>();
	ArrayList<Task> priorityThree = new ArrayList<Task>();
	ArrayList<Task> priorityFour = new ArrayList<Task>();
	ArrayList<ArrayList<Task>> queues = new ArrayList<ArrayList<Task>>();
	// Here are those four queues in an array
	
	// This is the current runQueue, used to detect changes.
	//ArrayList<Task> currentRunQueue;
	
	/**
	 * 
	 */
	public HighestPriorityFirstAge() {
		super();
		
		//currentRunQueue = this.runQueue;
		
		queues.add(priorityOne);
		queues.add(priorityTwo);
		queues.add(priorityThree);
		queues.add(priorityFour);
		
	}

	/**
	 * @param seed
	 */
	public HighestPriorityFirstAge(long seed) {
		super(seed);
		//prioritise();
		//currentRunQueue = this.runQueue;
	}

	/* (non-Javadoc)
	 * @see edu.sjsu.cs.SchedulingAlgorithm#nextTask()
	 */
	@Override
	public Task nextTask() {
		for(int i=0;i<queues.size();i++)
		{
			if(!queues.get(i).isEmpty())
			{
				return queues.get(i).get(0); // Return the item with the highest priority
			}
		}
		return null;
	}
	
	public String getName() {
		return "HighestPriorityFirst + Age";
	}
	
	public boolean nextStep(){
		Task t;
		boolean stillRunning;

		enqueue();
		
		//step correct task
		t = nextTask(); // Ask the algorithm for the next 
		
		if(t == null){
			slice += "*";
			atSlice++;
		}
		else{
			
			stillRunning = t.step(); // check if finished
			if(!stillRunning){
				queueRemove(t);
			}
			
			slice += t.getName();
			atSlice++;
			tick(t);
		}
		if(atSlice >= 100 && queueEmpty()){
			return false;
		}
		else{
			return true;
		}
	}
	private void tick(Task currentTask) {
		ArrayList<Task> aged = new ArrayList<Task>();
		for(ArrayList<Task> list:queues){
			for(Task task:list){
				if(currentTask == task){
					//skip
				}
				else{
					task.waitALittle();
					//Age check
					if(task.getTotalWait()-(task.getPriority()*5) > 5){
						aged.add(task);
						}
				}
			}
		}
		
		//reprioritized aged
		for(Task task:aged){
			queueRemove(task);
			task.promote();
			queueAdd(task);
		}
	}
	
	private void enqueue(){
		// Enqueue all the things!
		for(int i=lastNum; i<plist.size(); i++){
			if(Float.compare(plist.get(i).getArrivalTime(),(float)atSlice) <= 0){
				queueAdd(plist.get(i));
				lastNum = i+1;
			}
		}
	}

	/**
	 * So this *will* break if anything has an invalid priority
	 */
	private void queueAdd(Task j)
	{
			if(!queues.get(j.getPriority()-1).contains(j)){
				queues.get(j.getPriority()-1).add(j); // Adds items to their respective queues
			}
	}
	private void queueRemove(Task j)
	{
			if(queues.get(j.getPriority()-1).contains(j)){
				queues.get(j.getPriority()-1).remove(j); // Adds items to their respective queues
			}
			else{
				//removing a non exsistant element
			}
	}
	private boolean queueEmpty()
	{
		boolean empty = true;
		for(ArrayList<Task> t:queues)
			empty = t.isEmpty() && empty;
		return empty;
	}

}
