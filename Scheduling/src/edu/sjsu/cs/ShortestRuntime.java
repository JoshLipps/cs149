package edu.sjsu.cs;

/**
 * @author Adrian and Josh
 *
 */
public class ShortestRuntime extends SchedulingAlgorithm {
	
	ShortestRuntime(){
		super();
	}
	
	ShortestRuntime(long seed){
		super(seed);
	}
	
	public Task nextTask(){
		if(this.runQueue.isEmpty())
			return null;
		else
			return this.runQueue.get(0);
	}
	public String getName() {
		return "ShortestRuntime";
	}
	

}
