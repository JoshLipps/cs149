/**
 * 
 */
package edu.sjsu.cs;

/**
 * @author adrian
 */
public class RoundRobin extends SchedulingAlgorithm {

	/**
	 * 
	 */
	public RoundRobin() {
		super();
	}

	/**
	 * @param seed
	 */
	public RoundRobin(long seed) {
		super(seed);
	}

	/* (non-Javadoc)
	 * @see edu.sjsu.cs.SchedulingAlgorithm#nextTask()
	 */
	@Override
	public Task nextTask() {
		if(this.runQueue.isEmpty())
		{
			return null;
		}
		Task task = runQueue.remove(0);
		runQueue.add(task); // Remove from head and attach to tail
		return task;
	}
	public String getName() {
		return "RoundRobin";
	}

}
