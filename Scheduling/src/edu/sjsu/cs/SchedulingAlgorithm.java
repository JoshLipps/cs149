package edu.sjsu.cs;

import java.util.ArrayList;

/**
 * @author Adrian & Josh
 *
 */
public abstract class SchedulingAlgorithm {
	
	ArrayList<Task> runQueue;
	TaskQueue plist;
	int atSlice = 0,lastNum = 0;
	String slice = "";
	
	public SchedulingAlgorithm() {
		plist = new TaskQueue();
		runQueue = new ArrayList<Task>();
	}

	SchedulingAlgorithm(long seed){
		plist = new TaskQueue(seed);
		runQueue = new ArrayList<Task>();
	}


	public boolean nextStep(){
		Task t;
		boolean stillRunning;
		//check for new tasks to enqueue
		enqueue();
		//step correct task
		t = nextTask(); // Ask the algorithm for the next 
		
		if(t == null){
			slice += "*";
			atSlice++;
		}
		else{
			//System.out.println(t.getTotalRunTime());
			//System.out.println(t.getRuntime());
			stillRunning = t.step(); // check if finished
			//System.out.println(stillRunning);
			if(!stillRunning){
				//System.out.println(runQueue.size());
				//System.out.println("Remove" + t.getName());

				runQueue.remove(t);
				//for(int i =0;i<runQueue.size();i++)
					//System.out.print(runQueue.get(i).getName());
			}
			
			slice += t.getName();
			atSlice++;
			
			for(int i=0;i<runQueue.size();i++)
			{
				if(i == runQueue.indexOf(t)){
					//skip
				}
				else{
					runQueue.get(i).waitALittle();
				}
			}
		}
		//System.out.println(atSlice);
		//System.out.println(runQueue.size());
		if(atSlice >= 100 && runQueue.isEmpty()){
			return false;
		}
		else{
			return true;
		}
	}
	private void enqueue(){
		// Enqueue all the things!
		for(int i=lastNum; i<plist.size(); i++){
			if(Float.compare(plist.get(i).getArrivalTime(),(float)atSlice) <= 0){
				runQueue.add(plist.get(i));
				lastNum = i+1;
			}
		}
	}
	
	/**
	 * Ask the algorithm to tell us what the next task is!
	 * @return Returns the index of the next task to be run
	 */
	public abstract Task nextTask();

	public void run(){
		while(nextStep()){
			//this might not need anything here except debug notices
			//System.out.println(slice);
		}
		//taskList.calculate();
	}

	public float getAverageTurnaroundTime() {
		return plist.getAverageTurnaroundTime();
	}

	public float getAverageWaitingTime() {
		return plist.getAverageWaitingTime();
	}

	public float getAverageResponseTime() {
		return plist.getAverageResponseTime();
	}

	public float getAverageThroughput() {
		return (float)plist.size()/slice.length()*100;
	}

	public abstract String getName();

	public String getSlice() {
		return slice;
	}

	public String getPlist() {
		String s ="";
		for (int i=0;i<plist.size();i++){
			s += "Name: "+ plist.get(i).getName();
			s += " Arrival Time: "+ plist.get(i).getArrivalTime();
			s += " RunTime: " + plist.get(i).getRuntime();
			s += " Priority: " + plist.get(i).getPriority() +"\n";
		}
		return s;
	}


}
