package edu.sjsu.cs;

import java.util.ArrayList;
import java.lang.Float;
import java.util.Random;

/**
 * @author Adrian & Josh
 * implements list?
 */
public class TaskQueue {

	ArrayList<Task> taskList;
	Random rand;
	float averageTurnaroundTime = 0;
	float averageWaitingTime = 0;
	float averageResponseTime = 0;
	float averageThroughput = 0;
	
	TaskQueue() {
		taskList = new ArrayList<Task>();
		rand = new Random();
		fillTaskList();
	}
	
	
	TaskQueue(long seed) {
		taskList = new ArrayList<Task>();
		rand = new Random(seed);
		fillTaskList();
	}
	
	public boolean add(Task task){
		return taskList.add(task);
	}
	
	public Task get(int index)
	{
		return taskList.get(index);
	}
	
	public int size()
	{
		return taskList.size();
	}
	

	private void fillTaskList(){
		//generate ~10 tasks for this list
		float time = 0;
		float delta = 20;
		char name = 'A';

		while( Float.compare(time,100) < 0) {
			int priority  = rand.nextInt(4) + 1; // random priority 1-4
			int runTime = rand.nextInt(10) + 1; // random runtime 1-10
			float arrivalTime=time;
			
			// Make a task
			Task t = new Task(arrivalTime, runTime, priority, name+"" );
			
			taskList.add(t);

			//I hope there are only 26 tasks 
			name++;

			time += rand.nextFloat() * delta;
		}
		/**for(int i = 0;i<taskList.size();i++){
			System.out.print(taskList.get(i).getName());
			System.out.print(taskList.get(i).getRuntime());
		}
		System.out.println(taskList.size());*/
	}

	
	public float getAverageTurnaroundTime(){
		float f = 0;
		for(int i =0;i<taskList.size();i++){
			Task t = taskList.get(i);
			f += t.getTotalWait() +t.getTotalRunTime() ;
		}
		return f/taskList.size();
	}
	public float getAverageWaitingTime(){
		float f = 0;
		for(int i =0;i<taskList.size();i++){
			f += taskList.get(i).getTotalWait();
		}
		return f/taskList.size();
	}
	public float getAverageResponseTime(){
		float f = 0;
		for(int i =0;i<taskList.size();i++){
			Task t = taskList.get(i);
			f += t.getResponseTime() ;
		}
		return f/taskList.size();
	}

}
