/**
 * 
 */
package edu.sjsu.cs;

/**
 * @author Adrian
 *
 */
public class ShortestJobFirst extends SchedulingAlgorithm {
	Task lastTask; //Keep track of the last Task allowed to run
	
	ShortestJobFirst(){
		super();
	}
	
	ShortestJobFirst(long seed){
		super(seed);
	}
	
	public Task nextTask(){
		if(this.runQueue.isEmpty())
		{
			return null;
		}
		else if(this.runQueue.get(0).equals(lastTask))
		{
			return lastTask;
		}
		else
		{
			lastTask = this.runQueue.get(0);
			return lastTask;
		}
	}
	public String getName() {
		return "ShortestJobFirst";
	}
	

}
